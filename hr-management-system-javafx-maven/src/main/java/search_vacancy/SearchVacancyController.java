package search_vacancy;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import add_vacancy_new.AddVacancy;
import add_vacancy_new.Veh;
import admin_dashboard.AdminDashBoard;
import dashboard.DashBoard;
import db_operation.DbUtil;
import delete_vacancy.DeleteVacancy;
import edit_vacancy.EditVacancy;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import recruitment_dashboard.RecruitmentDashBoard;
import search_candidate.SearchCandidate;

public class SearchVacancyController implements Initializable {
	@FXML
	private TextField vacancyName;
	@FXML
	//private TextField description;
	//@FXML
	//private TextField hiringManager;
	//@FXML
//	private TextField noOfPositions;
//	@FXML
//	private ComboBox jobTitle;
//	@FXML
	private Button candidate;
	@FXML
	private Button vacancies;
	@FXML
	private Button add;
	@FXML
	private Button edit;
	@FXML
	private Button delete;
	@FXML
	//private Button search1;
	//@FXML
	private Button search;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button dashboard;
	@FXML
	private TableView<Veh> tableView;
	@FXML
	
	private TableColumn<Veh, String> col1;
	@FXML
	private TableColumn<Veh, String> col2;
	@FXML
	private TableColumn<Veh, String> col3;
	@FXML
	private TableColumn<Veh, String> col4;
	@FXML
	private TableColumn<Veh, String> col5;
	private ObservableList<Veh> data;
	

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		//ObservableList<String> list = FXCollections.observableArrayList("a", "b", "c");
	//	jobTitle.setItems(list);
		data=FXCollections.observableArrayList();

		col1.setCellValueFactory(new PropertyValueFactory<Veh,String>("vacancyName"));
		col2.setCellValueFactory(new PropertyValueFactory<Veh,String>("description"));
		col3.setCellValueFactory(new PropertyValueFactory<Veh,String>("hiringManager"));
		col4.setCellValueFactory(new PropertyValueFactory<Veh,String>("noOfPositions"));
		col5.setCellValueFactory(new PropertyValueFactory<Veh,String>("jobTitle"));
		buildData();
		
		FilteredList<Veh> filteredData=new FilteredList<>(data,b->true);
		vacancyName.textProperty().addListener((observable,oldValue,newValue)->{
		filteredData.setPredicate(Veh->{
			if(newValue.isEmpty() || newValue.isBlank() || newValue==null ) {
				return true;
			}
			String searchKeyword=newValue.toLowerCase();
			if(Veh.getVacancyName().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Veh.getJobTitle().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Veh.getDescription().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Veh.getHiringManager().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Veh.getNoOfPositions().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else
				return false;
			
		});
		});
		

		
		SortedList<Veh> sortedData =new SortedList<>(filteredData);

		sortedData.comparatorProperty().bind(tableView.comparatorProperty());
		tableView.setItems(sortedData);
		
	}
	public void buildData()
	{
		try {
			data=FXCollections.observableArrayList();
			String query = "Select*from vacancy";
			System.out.println(query);
			ResultSet resultSet = DbUtil.executeQueryGetResult(query);
			while (resultSet.next()) {
				Veh ve=new Veh();
				ve.vacancyName.set(resultSet.getString(2));
				ve.description.set(resultSet.getString(3));
				ve.hiringManager.set(resultSet.getString(4));
				ve.noOfPositions.set(resultSet.getString(5));
				ve.jobTitle.set(resultSet.getString(6));
				data.add(ve);
			}
			tableView.setItems(data);
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	public void admin(ActionEvent event) {
		new AdminDashBoard().show();
	}
	public void recruitment(ActionEvent event) {
		new RecruitmentDashBoard().show();
	}
	public void dashboard(ActionEvent event) {
		new DashBoard().show();
	}
	public void candidate(ActionEvent event) {
		new SearchCandidate().show();
	}
	public void vacancies(ActionEvent event) {
		new SearchVacancy().show();
	}
	public void add(ActionEvent event) {
		new AddVacancy().show();
		
	}
	public void edit(ActionEvent event) {
		new EditVacancy().show();
		
	}
	public void delete(ActionEvent event) {
		new DeleteVacancy().show();
		
	}
	
	//public void search(ActionEvent event)  {
//		System.out.println(vacancyName.getText());
//		System.out.println(description.getText());
//		System.out.println(hiringManager.getText());
//		System.out.println(noOfPositions.getText());
//		System.out.println(jobTitle.getPromptText());
//		data=FXCollections.observableArrayList();
//		
//		FilteredList<Veh> filteredData=new FilteredList<>(data,b->true);
//		vacancyName.textProperty().addListener((observable,oldValue,newValue)->{
//		filteredData.setPredicate(Veh->{
//			if(newValue.isEmpty() || newValue.isBlank() || newValue==null ) {
//				return true;
//			}
//			String searchKeyword=newValue.toLowerCase();
//			if(Veh.getVacancyName().toLowerCase().indexOf(searchKeyword)> -1) {
//				return true;
//			}else if(Veh.getJobTitle().toLowerCase().indexOf(searchKeyword)> -1) {
//				return true;
//			}else if(Veh.getDescription().toLowerCase().indexOf(searchKeyword)> -1) {
//				return true;
//			}else if(Veh.getHiringManager().toLowerCase().indexOf(searchKeyword)> -1) {
//				return true;
//			}else if(Veh.getNoOfPositions().toLowerCase().indexOf(searchKeyword)> -1) {
//				return true;
//			}else
//				return false;
//			
//		});
//		});
//
//		
//		SortedList<Veh> sortedData =new SortedList<>(filteredData);
//		sortedData.comparatorProperty().bind(tableView.comparatorProperty());
//		tableView.setItems(sortedData);
//	}

}
