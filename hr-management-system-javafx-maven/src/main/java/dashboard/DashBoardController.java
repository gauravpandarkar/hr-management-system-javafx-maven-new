package dashboard;

import admin_dashboard.AdminDashBoard;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import recruitment_dashboard.RecruitmentDashBoard;

public class DashBoardController {
	@FXML
	private Button search;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	

	public void admin(ActionEvent event) {
		new AdminDashBoard().show();

	}

	public void recruitment(ActionEvent event) {
		new RecruitmentDashBoard().show();

	}
}
