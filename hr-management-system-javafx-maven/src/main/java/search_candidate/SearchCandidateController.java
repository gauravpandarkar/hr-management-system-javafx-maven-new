package search_candidate;

import java.io.File;
import java.net.URL;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import add_candidate.AddCandidate;
import add_candidate.Candidate;
import add_vacancy_new.Veh;
import admin_dashboard.AdminDashBoard;
import dashboard.DashBoard;
import db_operation.DbUtil;
import delete_candidate.DeleteCandidate;
import edit_candidate.EditCandidate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import login.Login;
import recruitment_dashboard.RecruitmentDashBoard;
import search_vacancy.SearchVacancy;

public class SearchCandidateController implements Initializable {
	@FXML
	private Button search1;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button candidate;
	@FXML
	private Button vacancies;
	@FXML

	private Button dashboard;
	@FXML
	private Button add;
	
	@FXML
	private TextField candidateName;
	@FXML
	private Button delete;
	@FXML
	private Button edit;
	//@FXML
//	private ComboBox jobTitle;
//	@FXML
//	private ComboBox vacancy;
//	@FXML
//	private ComboBox hiringManager;
//	@FXML
//	private ComboBox status;
//	@FXML
//	private ComboBox methodOfApplication;
//	@FXML
//	private DatePicker applicationDateFrom;
	 @FXML
		private TableView<Candidate> tableView;
		@FXML

		private TableColumn<Candidate, String> col1;
		@FXML
		private TableColumn<Candidate, String> col2;
		@FXML
		private TableColumn<Candidate, String> col3;
		@FXML
		private TableColumn<Candidate, String> col4;
		@FXML
		private TableColumn<Candidate, String> col5;
		@FXML
		private TableColumn<Candidate, String> col6;
		@FXML
		private TableColumn<Candidate, String> col7;
		@FXML
		private TableColumn<Candidate, String> col8;
		@FXML
		private TableColumn<Candidate, String> col9;
		

		private ObservableList<Candidate> data;

//	@FXML
//	private DatePicker applicationDateTo;
//	public void getDateFrom(ActionEvent event) {
//		LocalDate myDate=applicationDateFrom.getValue();
//		String myFormattedDate=myDate.format(DateTimeFormatter.ofPattern("MM-dd-yy"));
//	
//	}
//	public void getDateTo(ActionEvent event) {
//		LocalDate myDateTo=applicationDateTo.getValue();
//		String myFormattedDate=myDateTo.format(DateTimeFormatter.ofPattern("MM-dd-yy"));
//	
//	}
	@Override
	public void initialize(URL url, ResourceBundle rb) {
//	ObservableList<String> list = FXCollections.observableArrayList("Account Assistant", "Chief Executive Officer", "Chief Financial Officer","Chief Content Officer","Content Specialist","Database Administrator","HR Manager");
//	jobTitle.setItems(list);
//	ObservableList<String> list2 = FXCollections.observableArrayList("Junior Account Assistant", "Sales Representative", "Senior QA Lead","Senior Support Specialist","Software Engineer");
//		vacancy.setItems(list2);
//		ObservableList<String> list3 = FXCollections.observableArrayList("a", "b", "c");
//		hiringManager.setItems(list3);
//		ObservableList<String> list4 = FXCollections.observableArrayList("Interview Scheduled", "Interview Passed", "Interview Failed","Job Offered","Offer Declined","Hired");
//		status.setItems(list4);
		data=FXCollections.observableArrayList();

		
		col1.setCellValueFactory(new PropertyValueFactory<Candidate, String>("firstName"));
		col2.setCellValueFactory(new PropertyValueFactory<Candidate, String>("middleName"));
		col3.setCellValueFactory(new PropertyValueFactory<Candidate, String>("lastName"));

		col4.setCellValueFactory(new PropertyValueFactory<Candidate, String>("vacancy"));
		col5.setCellValueFactory(new PropertyValueFactory<Candidate, String>("email"));
		col6.setCellValueFactory(new PropertyValueFactory<Candidate, String>("contactNumber"));
		col7.setCellValueFactory(new PropertyValueFactory<Candidate, String>("keyword"));
		col8.setCellValueFactory(new PropertyValueFactory<Candidate, String>("date"));

		col9.setCellValueFactory(new PropertyValueFactory<Candidate, String>("note"));
	buildData();
		
		FilteredList<Candidate> filteredData=new FilteredList<>(data,b->true);
		candidateName.textProperty().addListener((observable,oldValue,newValue)->{
		filteredData.setPredicate(Candidate->{
			if(newValue.isEmpty() || newValue.isBlank() || newValue==null ) {
				return true;
			}
			String searchKeyword=newValue.toLowerCase();
			if(Candidate.getFirstName().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Candidate.getMiddleName().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Candidate.getLastName().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Candidate.getVacancy().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Candidate.getEmail().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Candidate.getContactNumber().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Candidate.getKeyword().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Candidate.getDate().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Candidate.getNote().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else  {
				return false;
			}
				
			
		});
		});
		

		
		SortedList<Candidate> sortedData =new SortedList<>(filteredData);

		sortedData.comparatorProperty().bind(tableView.comparatorProperty());
		tableView.setItems(sortedData);
		
		
	}
	public void buildData()
	{
		try {
			data=FXCollections.observableArrayList();
			String query = "Select*from recruitment";
			System.out.println(query);
			ResultSet resultSet = DbUtil.executeQueryGetResult(query);
			while (resultSet.next()) {
				Candidate ca=new Candidate();
				ca.firstName.set(resultSet.getString(1));
				ca.middleName.set(resultSet.getString(2));
				ca.lastName.set(resultSet.getString(3));
				ca.vacancy.set(resultSet.getString(4));
				ca.email.set(resultSet.getString(5));
				ca.contactNumber.set(resultSet.getString(6));

				ca.keyword.set(resultSet.getString(7));

				ca.date.set(resultSet.getString(8));

				ca.note.set(resultSet.getString(9));

				data.add(ca);
			}
			tableView.setItems(data);
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	

	public void admin(ActionEvent event) {

		new AdminDashBoard().show();

	}

	public void recruitment(ActionEvent event) {
		new RecruitmentDashBoard().show();

	}

	public void candidate(ActionEvent event) {
		new SearchCandidate().show();

	}

	public void vacancies(ActionEvent event) {
		new SearchVacancy().show();

	}

	public void dashboard(ActionEvent event) {
		new DashBoard().show();

	}

//	public void search(ActionEvent event) {
//
//	}

	public void add(ActionEvent event) {
		new AddCandidate().show();

	}
	public void delete(ActionEvent event) {
		new DeleteCandidate().show();

	}
	public void edit(ActionEvent event) {
		new EditCandidate().show();

	}
		
//	public void reset(ActionEvent event) {
//		
//
//	}

}
