package add_vacancy_new;

import javafx.beans.property.SimpleStringProperty;

public class Veh {
	public SimpleStringProperty vacancyName=new SimpleStringProperty();
	public SimpleStringProperty description=new SimpleStringProperty();

	public SimpleStringProperty hiringManager=new SimpleStringProperty();

	public SimpleStringProperty noOfPositions=new SimpleStringProperty();
	public SimpleStringProperty jobTitle=new SimpleStringProperty();
	
	  public String getVacancyName(){
	       return vacancyName.get();
	   }

	   public String getDescription(){
	       return description.get();
	   }

	   public String getHiringManager(){
	       return hiringManager.get();
	   }

	   public String getNoOfPositions(){
	       return noOfPositions.get();
	   }

	   public String getJobTitle(){
	       return jobTitle.get();
	   }

	  




}
