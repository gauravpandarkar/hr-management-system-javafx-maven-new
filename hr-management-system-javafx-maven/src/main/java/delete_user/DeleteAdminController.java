package delete_user;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import add.User;
import admin_dashboard.AdminDashBoard;
import dashboard.DashBoard;
import db_operation.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import recruitment_dashboard.RecruitmentDashBoard;

public class DeleteAdminController implements Initializable {
	@FXML
	private TextField userName;
	@FXML
	private Button cancel;
	@FXML
	private Button delete;
	@FXML
	private Button search;
	
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button dashboard;
	@FXML
	private TableView<User> tableView;
	@FXML

	private TableColumn<User, String> col1;
	@FXML
	private TableColumn<User, String> col2;
	@FXML
	private TableColumn<User, String> col3;
	@FXML
	private TableColumn<User, String> col4;
	@FXML
	private TableColumn<User, String> col5;
	@FXML
	private TableColumn<User, String> col6;
	private ObservableList<User> data;
	
	public void admin(ActionEvent event) {

		new AdminDashBoard().show();

	}

	public void recruitment(ActionEvent event) {
		new RecruitmentDashBoard().show();

	}

	public void dashboard(ActionEvent event) {
		new DashBoard().show();

	}

	public void cancel(ActionEvent event) {
		new AdminDashBoard().show();

	}
	public void delete(ActionEvent event) throws SQLException {
		System.out.println(userName.getText());
		 if (userName.getText().isEmpty() ) {
             Alert alert = new Alert(Alert.AlertType.ERROR);
             alert.setTitle("Error");
             alert.setHeaderText(null);
             alert.setContentText("Please fill in all fields.");
             alert.showAndWait();
             return;
         }
		 String query1="select count(*) from user where userName='" +userName.getText() + "';";
		  ResultSet rs1=DbUtil.executeQueryGetResult(query1);
		  rs1.next();
		  if(rs1.getInt(1)==1) {

		String query = " delete from user where userName ='" + userName.getText() + "';";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur delete controller " + event.getEventType().getName());
		
		buildData();
		  }else {
			  Alert alert = new Alert(Alert.AlertType.ERROR);
              alert.setTitle("Error");
              alert.setHeaderText(null);
              alert.setContentText("check userName which you want to delete is present or not");
              alert.showAndWait();
              return;
		  }

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		col1.setCellValueFactory(new PropertyValueFactory<User, String>("userRole"));
		col2.setCellValueFactory(new PropertyValueFactory<User, String>("employeeName"));
		col3.setCellValueFactory(new PropertyValueFactory<User, String>("status"));

		col4.setCellValueFactory(new PropertyValueFactory<User, String>("userName"));
		col5.setCellValueFactory(new PropertyValueFactory<User, String>("password"));
		col6.setCellValueFactory(new PropertyValueFactory<User, String>("confirmPassword"));
		buildData();
		
	}
	public void buildData() {
		try {
			data = FXCollections.observableArrayList();
			String query = "Select*from user";
			System.out.println(query);
			ResultSet resultSet = DbUtil.executeQueryGetResult(query);
			while (resultSet.next()) {
				User user = new User();
				user.userRole.set(resultSet.getString(3));
				user.employeeName.set(resultSet.getString(4));
				user.status.set(resultSet.getString(5));
				user.userName.set(resultSet.getString(1));
				user.password.set(resultSet.getString(2));
				user.confirmPassword.set(resultSet.getString(6));

				

				data.add(user);
			}
			tableView.setItems(data);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	

}
